# Build stage
FROM golang:alpine AS builder
    # FROM golang:1.22.0-alpine3.18 AS builder
WORKDIR /app
# pre-copy/cache go.mod for pre-downloading dependencies and only redownloading them in subsequent builds if they change
COPY . .
RUN go mod tidy
WORKDIR /app/cmd
RUN go build -o main main.go

FROM alpine:latest
    # FROM alpine:3.18
WORKDIR /app
COPY --from=builder /app/cmd/main .
WORKDIR /app/config
#COPY --from=builder /app/config/local.yaml .
WORKDIR /app/cmd/public
COPY --from=builder /app/cmd/public/swagger.json .
ENV CONFIG_PATH=./config/local.yaml
WORKDIR /app
EXPOSE 8080
CMD ["/app/main"]
**VacancyCollector** - собирает информацию о вакансиях с сайта https://career.habr.com/ посредствам парсинга web-страниц и сохраняет её в БД.

Задействованные инструменты и технологии:
 - Golang
 - Swagger
 - github.com/go-chi/chi/v5
 - Selenium
 - PostgreSQL
 - SQLite
 - Docker, docker compose
 

// docker run --rm -p 4444:4444 --shm-size="2g" -e SE_VNC_NO_PASSWORD=1  selenium/standalone-chrome
package main

import (
	"context"
	"fmt"
	"github.com/go-chi/chi/v5"
	"github.com/go-chi/chi/v5/middleware"
	"github.com/go-chi/render"
	"github.com/tebeka/selenium"
	"github.com/tebeka/selenium/chrome"
	"gitlab.com/Rokzish/webAPI/internal/config"
	"gitlab.com/Rokzish/webAPI/internal/http-server/handlers"
	"gitlab.com/Rokzish/webAPI/internal/lib/logger/sl"
	"gitlab.com/Rokzish/webAPI/internal/lib/logger/slogpretty"
	"gitlab.com/Rokzish/webAPI/internal/service"
	"gitlab.com/Rokzish/webAPI/internal/storage/postresql"
	"gitlab.com/Rokzish/webAPI/internal/storage/sqlite"
	"golang.org/x/exp/slog"
	"net/http"
	"os"
	"os/signal"
	"text/template"
	"time"
)

const (
	swaggerTemplate = `<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <script src="//unpkg.com/swagger-ui-dist@3/swagger-ui-standalone-preset.js"></script>
    <!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/swagger-ui/3.22.1/swagger-ui-standalone-preset.js"></script> -->
    <script src="//unpkg.com/swagger-ui-dist@3/swagger-ui-bundle.js"></script>
    <!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/swagger-ui/3.22.1/swagger-ui-bundle.js"></script> -->
    <link rel="stylesheet" href="//unpkg.com/swagger-ui-dist@3/swagger-ui.css" />
    <!-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/swagger-ui/3.22.1/swagger-ui.css" /> -->
	<style>
		body {
			margin: 0;
		}
	</style>
    <title>Swagger</title>
</head>
<body>
    <div id="swagger-ui"></div>
    <script>
        window.onload = function() {
          SwaggerUIBundle({
            url: "/public/swagger.json?{{.Time}}",
            dom_id: '#swagger-ui',
            presets: [
              SwaggerUIBundle.presets.apis,
              SwaggerUIStandalonePreset
            ],
            layout: "StandaloneLayout"
          })
        }
    </script>
</body>
</html>
`
)

func swaggerUI(w http.ResponseWriter, _ *http.Request) {
	w.Header().Set("Content-Type", "text/html; charset=utf-8")
	tmpl, err := template.New("swagger").Parse(swaggerTemplate)
	if err != nil {
		return
	}
	err = tmpl.Execute(w, struct {
		Time int64
	}{
		Time: time.Now().Unix(),
	})
	if err != nil {
		return
	}
}

func main() {
	var err error
	// config cleanenv
	cfg := config.MustLoad()
	fmt.Println(cfg)
	//  init logger: slog
	opts := slogpretty.PrettyHandlerOption{
		SlogOpts: &slog.HandlerOptions{
			Level: slog.LevelDebug,
		}}
	log := slog.New(opts.NewPrettyHandler(os.Stdout))

	log.Info("starting webAPI")
	log.Debug("debug messages are enabled")
	log.Error("error messages are enabled")

	// init db route
	var storage service.Storage
	if cfg.StoragePath != "" {
		// init storage: sqlite3
		var storageSqlite *sqlite.Storage
		for i := 0; i < 15; i++ {
			storageSqlite, err = sqlite.New(cfg.StoragePath)
			if err != nil && i == 14 {
				log.Error("failed to init storage:", sl.Err(err))
				os.Exit(1)
			} else if err == nil {
				break
			}
			time.Sleep(500 * time.Millisecond)
		}
		storage = storageSqlite
		log.Info("sqlite3 prepared")
	} else if cfg.DBUrl != "" {
		// init storage: postgresql
		var storagePg *postresql.Storage
		for i := 0; i < 15; i++ {
			storagePg, err = postresql.New(cfg.DBUrl)
			if err != nil && i == 14 {
				log.Error("failed to init storage:", sl.Err(err))
				os.Exit(1)
			} else if err == nil {
				break
			}
			time.Sleep(500 * time.Millisecond)
		}
		storage = storagePg
		log.Info("postgresql connected")
	} else {
		log.Error("invalid config")
		os.Exit(1)
	}
	defer storage.Close()

	// init selenium
	caps := selenium.Capabilities{
		"browserName": "chrome",
	}
	chrCaps := chrome.Capabilities{
		W3C: true,
	}
	caps.AddChrome(chrCaps)

	var wd selenium.WebDriver
	//urlPrefix := "http://selenium:4444/wd/hub"
	// urlPrefix := selenium.DefaultURLPrefix
	for i := 1; i < 15; i++ {
		wd, err = selenium.NewRemote(caps, cfg.SeleniumUrl)
		if err != nil && i == 14 {
			log.Error("failed to create remote client:", sl.Err(err))
		} else if err == nil {
			break
		}
		time.Sleep(500 * time.Millisecond)
	}

	vacancyHandler := handlers.New(log, service.NewVacancyController(storage, wd))

	// init router: chi
	router := chi.NewRouter()

	router.Use(middleware.RequestID)
	router.Use(middleware.Logger)
	router.Use(middleware.Recoverer)
	router.Use(middleware.URLFormat)
	router.Use(render.SetContentType(render.ContentTypeJSON))

	router.Route("/vacancy", func(router chi.Router) {
		router.Post("/search", vacancyHandler.SearchVacancyByQuery)
		router.Post("/get", vacancyHandler.GetVacancyByID)
		router.Get("/list", vacancyHandler.GetListVacancy)
		router.Delete("/delete", vacancyHandler.DeleteVacancyByID)
	})

	//SwaggerUI
	router.Get("/swagger", swaggerUI)
	router.Get("/public/*", func(w http.ResponseWriter, r *http.Request) {
		http.StripPrefix("/public/", http.FileServer(http.Dir("./cmd/public"))).ServeHTTP(w, r)
	})

	srv := &http.Server{
		Addr:    cfg.HTTPServer.Address,
		Handler: router,
	}

	// Запуск веб-сервера в отдельном горутине
	go func() {
		log.Info("starting server", slog.String("address", cfg.HTTPServer.Address))
		if err := srv.ListenAndServe(); err != nil && err != http.ErrServerClosed {
			log.Error("failed to start server", sl.Err(err))
			os.Exit(1)
		}
	}()

	// Ожидание сигнала для начала завершения работы
	quit := make(chan os.Signal, 1)
	signal.Notify(quit, os.Interrupt)
	<-quit
	log.Info("Shutdown Server ...")

	// Установка тайм-аута для завершения работы
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()
	if err := srv.Shutdown(ctx); err != nil {
		log.Error("Server Shutdown:", sl.Err(err))
		os.Exit(1)
	}

	log.Info("Server exiting")

	_ = wd.Quit()
}

package main

import (
	"gitlab.com/Rokzish/webAPI/internal/http-server/handlers"
)

func init() {
	_ = vacancySearchReq{}
	_ = vacancySearchRes{}
	_ = vacancyDeleteReq{}
	_ = vacancyGetReq{}
	_ = errFoundResp{}
	_ = errInputResp{}
}

// swagger:route POST /vacancy/search vacancy vacancySearchReq
// Обновление списка вакансий.
// responses:
//   200: vacancySearchRes
//   400: errInputResp

// swagger:parameters vacancySearchReq
type vacancySearchReq struct {
	// in:body
	// required:true
	Body handlers.ReqBodyQuery
}

// Количество найденных вакансий.
// swagger:response vacancySearchRes
type vacancySearchRes struct {
	// in:body
	Body handlers.ResBodyVacCount
}

// swagger:route DELETE /vacancy/delete vacancy vacancyDeleteReq
// Удалить вакансию по ID.
// responses:
//   404: errFoundResp
//   400: errInputResp

// swagger:parameters vacancyDeleteReq
type vacancyDeleteReq struct {
	// in:body
	// required:true
	Body handlers.ReqBodyID
}

// swagger:route POST /vacancy/get vacancy vacancyGetReq
// Получение вакансию по ID.
// responses:
//   404: errFoundResp
//   400: errInputResp

// swagger:parameters vacancyGetReq
type vacancyGetReq struct {
	// in:body
	// required:true
	Body handlers.ReqBodyID
}

// swagger:route GET /vacancy/list vacancy vacancy
// Получение списка всех доступных вакансий.
// responses:
//   404: errFoundResp

// Not found.
// swagger:response errFoundResp
type errFoundResp struct{}

// Invalid input.
// swagger:response errInputResp
type errInputResp struct{}

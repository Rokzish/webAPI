package handlers

import (
	"encoding/json"
	"github.com/go-chi/chi/v5/middleware"
	"gitlab.com/Rokzish/webAPI/internal/lib/logger/sl"
	"gitlab.com/Rokzish/webAPI/internal/model"
	"golang.org/x/exp/slog"
	"net/http"
)

type ReqBodyQuery struct {
	Query string `json:"query"`
}
type ReqBodyID struct {
	ID int `json:"id"`
}

type ResBodyVacCount struct {
	Count int `json:"Vacancy count"`
}

type VacancyService interface {
	UpdateVacancyList(log *slog.Logger, query string) (int, error)
	DeleteVacancy(id int) error
	GetVacancyByID(id int) (model.Vacancy, error)
	GetList() ([]model.Vacancy, error)
}

type VacancyHandler struct {
	service VacancyService
	log     *slog.Logger
}

func New(log *slog.Logger, service VacancyService) *VacancyHandler {
	return &VacancyHandler{service: service, log: log}
}

func (v *VacancyHandler) SearchVacancyByQuery(w http.ResponseWriter, r *http.Request) {

	const op = "handler.SearchVacancyByQuery"

	log := v.log.With(
		slog.String("op", op),
		slog.String("request_id", middleware.GetReqID(r.Context())),
	)

	var (
		query        ReqBodyQuery
		err          error
		vacancyCount ResBodyVacCount
	)

	err = json.NewDecoder(r.Body).Decode(&query)
	if err != nil {
		log.Error("failed to decode request body", sl.Err(err))
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}
	vacancyCount.Count, err = v.service.UpdateVacancyList(v.log, query.Query)
	if err != nil {
		log.Error("failed to update vacancy list", sl.Err(err))
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	w.Header().Set("Content-Type", "application/json;charset=utf-8") // выставляем заголовки, что отправляем json в utf8
	err = json.NewEncoder(w).Encode(vacancyCount)
	if err != nil {
		log.Error("failed to display result", sl.Err(err))
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
}

func (v *VacancyHandler) DeleteVacancyByID(w http.ResponseWriter, r *http.Request) {
	const op = "handlers.DeleteVacancyByID"

	var (
		err error
		id  ReqBodyID
	)

	log := v.log.With(
		slog.String("op", op),
		slog.String("request_id", middleware.GetReqID(r.Context())),
	)

	err = json.NewDecoder(r.Body).Decode(&id)
	if err != nil {
		log.Error("failed to decode request body", sl.Err(err))
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}
	err = v.service.DeleteVacancy(id.ID)
	if err != nil && err.Error() != "vacancy with this ID not found" {
		log.Error("failed to delete vacancy", sl.Err(err))
		http.Error(w, err.Error(), http.StatusNotFound)
		return
	}

	w.Header().Set("Content-Type", "application/json;charset=utf-8")
	err = json.NewEncoder(w).Encode("Deletion successful")

	if err != nil {
		log.Error("failed to display result", sl.Err(err))
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
}

func (v *VacancyHandler) GetVacancyByID(w http.ResponseWriter, r *http.Request) {
	const op = "handlers.GetVacancyByID"

	log := v.log.With(
		slog.String("op", op),
		slog.String("request_id", middleware.GetReqID(r.Context())),
	)

	var (
		vac model.Vacancy
		err error
		id  ReqBodyID
	)

	err = json.NewDecoder(r.Body).Decode(&id)
	if err != nil {
		log.Error("failed to decode request body", sl.Err(err))
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}
	vac, err = v.service.GetVacancyByID(id.ID)
	if err != nil {
		log.Error("failed to get vacancy by id", sl.Err(err))
		http.Error(w, err.Error(), http.StatusNotFound)
		return
	}

	w.Header().Set("Content-Type", "application/json;charset=utf-8")
	err = json.NewEncoder(w).Encode(vac)

	if err != nil {
		log.Error("failed to display result", sl.Err(err))
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
}

func (v *VacancyHandler) GetListVacancy(w http.ResponseWriter, r *http.Request) {
	const op = "handlers.GetListVacancy"

	log := v.log.With(
		slog.String("op", op),
		slog.String("request_id", middleware.GetReqID(r.Context())),
	)

	var (
		vacancies []model.Vacancy
		err       error
	)
	w.Header().Set("Content-Type", "application/json;charset=utf-8")
	vacancies, err = v.service.GetList()
	if err != nil {
		log.Error("failed to get vacancy list", sl.Err(err))
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	if len(vacancies) < 1 {
		err := json.NewEncoder(w).Encode("List is empty")
		if err != nil {
			log.Error("failed to display result", sl.Err(err))
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}
		return
	}

	err = json.NewEncoder(w).Encode(vacancies)
	if err != nil {
		log.Error("failed to display result", sl.Err(err))
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
}

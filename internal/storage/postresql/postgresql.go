package postresql

import (
	"context"
	"database/sql"
	"errors"
	"fmt"
	"github.com/jackc/pgx/v5/pgxpool"
	"github.com/jackc/pgx/v5/stdlib"
	_ "github.com/jackc/pgx/v5/stdlib"
	"gitlab.com/Rokzish/webAPI/internal/model"
)

type Storage struct {
	db *sql.DB
}

func New(dbUrl string) (*Storage, error) {
	const op = "storage.postgres.New"

	/*db, err := sql.Open("pgx", dbUrl)
	if err != nil {
		return nil, fmt.Errorf("%s: %w", op, err)
	}*/

	pool, err := pgxpool.New(context.Background(), dbUrl)

	if err != nil {
		return nil, fmt.Errorf("%s: %w", op, err)
	}

	db := stdlib.OpenDBFromPool(pool)

	stmt, err := db.Prepare(`
		CREATE TABLE IF NOT EXISTS vacancy(
		    id SERIAL PRIMARY KEY,
		    link TEXT NOT NULL UNIQUE,
		    dataPosted TEXT DEFAULT 'N/A',
		    description TEXT DEFAULT 'N/A',
	    	employmentType TEXT DEFAULT 'N/A',
	    	organizationSite TEXT DEFAULT 'N/A',
	    	organizationName TEXT DEFAULT 'N/A',
	    	Title TEXT DEFAULT 'N/A',
	    	validThrough TEXT DEFAULT 'N/A',
	    	currency TEXT DEFAULT 'N/A',
	    	maxValue DECIMAL DEFAULT 0, 
			minValue DECIMAL DEFAULT 0
		);
	`)
	if err != nil {
		return nil, fmt.Errorf("%s: prepare statment: %w", op, err)
	}
	defer stmt.Close()

	_, err = stmt.Exec()
	if err != nil {
		return nil, fmt.Errorf("%s execute statment: %w", op, err)
	}

	_, err = db.Exec("TRUNCATE TABLE vacancy RESTART IDENTITY ;")
	if err != nil {
		return nil, fmt.Errorf("%s: truncate statment %w", op, err)
	}

	return &Storage{db: db}, nil

}

func (s *Storage) CreateVac(vacancy model.Vacancy) error {
	const op = "storage.postgres.CreateVac"

	stmt, err := s.db.Prepare(`
		INSERT INTO vacancy(
		                    link, 
		                    dataposted, 
		                    description, 
		                    employmentType, 
		                    organizationSite, 
		                    organizationName, 
		                    title, 
		                    validthrough, 
		                    currency, 
		                    maxvalue, 
		                    minvalue)
		VALUES ($1,$2,$3, $4, $5, $6, $7, $8, $9, $10, $11);
	`)
	if err != nil {
		return fmt.Errorf("%s: prepare statment: %w", op, err)
	}

	defer stmt.Close()

	_, err = stmt.Exec(
		vacancy.Link,
		vacancy.DatePosted,
		vacancy.Description,
		vacancy.EmploymentType,
		vacancy.OrganizationSite,
		vacancy.OrganizationName,
		vacancy.Title,
		vacancy.ValidThrough,
		vacancy.BaseSalary.Currency,
		vacancy.BaseSalary.Value.MaxValue,
		vacancy.BaseSalary.Value.MinValue,
	)
	if err != nil {
		return fmt.Errorf("%s execute statment: %w", op, err)
	}

	return nil
}

func (s *Storage) GetByID(id int) (model.Vacancy, error) {
	const op = "storage.postgres.GetByID"

	stmt, err := s.db.Prepare(`
		SELECT 	link, 
				dataposted, 
				description, 
				employmentType, 
				organizationSite, 
				organizationName, 
				title, 
				validthrough, 
		        currency, 
		        maxvalue, 
		        minvalue
		FROM vacancy
		WHERE id = $1;
	`)
	if err != nil {
		return model.Vacancy{}, fmt.Errorf("%s: prepare statment: %w", op, err)

	}
	defer stmt.Close()

	var resVac model.Vacancy
	err = stmt.QueryRow(id).Scan(
		&resVac.Link,
		&resVac.DatePosted,
		&resVac.Description,
		&resVac.EmploymentType,
		&resVac.OrganizationSite,
		&resVac.OrganizationName,
		&resVac.Title,
		&resVac.ValidThrough,
		&resVac.BaseSalary.Currency,
		&resVac.BaseSalary.Value.MaxValue,
		&resVac.BaseSalary.Value.MinValue)

	if err != nil {
		if errors.Is(err, sql.ErrNoRows) {
			return model.Vacancy{}, fmt.Errorf("%s: vacancy not found", op)
		}
		return model.Vacancy{}, fmt.Errorf("%s execute statment: %w", op, err)
	}

	return resVac, nil
}

func (s *Storage) GetList() ([]model.Vacancy, error) {
	const op = "storage.postgres.GetList"

	stmt, err := s.db.Prepare(`
		SELECT 	link, 
				dataposted, 
				description, 
				employmentType, 
				organizationSite, 
				organizationName, 
				title, 
				validthrough, 
		        currency, 
		        maxvalue, 
		        minvalue
		FROM vacancy;
	`)
	if err != nil {
		return nil, fmt.Errorf("%s: prepare statment: %w", op, err)
	}
	defer stmt.Close()

	resVac := []model.Vacancy{}

	rows, err := stmt.Query()
	if err != nil {
		if errors.Is(err, sql.ErrNoRows) {
			return nil, fmt.Errorf("%s: vacancy not found", op)
		}
		return nil, fmt.Errorf("%s execute statment: %w", op, err)
	}
	defer rows.Close()
	for rows.Next() {
		tmpVac := model.Vacancy{}
		err := rows.Scan(
			&tmpVac.Link,
			&tmpVac.DatePosted,
			&tmpVac.Description,
			&tmpVac.EmploymentType,
			&tmpVac.OrganizationSite,
			&tmpVac.OrganizationName,
			&tmpVac.Title,
			&tmpVac.ValidThrough,
			&tmpVac.BaseSalary.Currency,
			&tmpVac.BaseSalary.Value.MaxValue,
			&tmpVac.BaseSalary.Value.MinValue)
		if err != nil {
			return nil, fmt.Errorf("%s scan error: %w", op, err)
		}
		resVac = append(resVac, tmpVac)
	}

	if err := rows.Err(); err != nil {
		return nil, fmt.Errorf("%s iteration error: %w", op, err)
	}

	return resVac, nil

}

func (s *Storage) Delete(id int) error {

	const op = "storage.postgres.Delete"

	stmt, err := s.db.Prepare("DELETE FROM vacancy WHERE id = $1")
	if err != nil {
		return fmt.Errorf("%s: prepare statment: %w", op, err)
	}
	defer stmt.Close()

	res, err := stmt.Exec(id)
	if err != nil {
		return fmt.Errorf("%s execute statment: %w", op, err)
	}

	count, err := res.RowsAffected()
	if count == 0 {
		return fmt.Errorf("vacancy with this ID not found")
	}

	return nil
}

func (s *Storage) Close() error {
	const op = "storage.sqlite.Close"
	err := s.db.Close()
	if err != nil {
		return fmt.Errorf("%s: %w", op, err)
	}
	return nil
}

package sqlite

import (
	"database/sql"
	"errors"
	"fmt"
	_ "github.com/mattn/go-sqlite3"
	"gitlab.com/Rokzish/webAPI/internal/model"
)

type Storage struct {
	db *sql.DB
}

func New(storagePath string) (*Storage, error) {
	const op = "storage.sqlite.New"

	db, err := sql.Open("sqlite3", storagePath)
	if err != nil {
		return nil, fmt.Errorf("%s: %w", op, err)
	}
	stmt, err := db.Prepare(`
	CREATE TABLE IF NOT EXISTS vacancy(
	    id INTEGER PRIMARY KEY,
	    link TEXT NOT NULL UNIQUE,
	    dataPosted 	TEXT,
	    description TEXT,
	    employmentType TEXT,
	    hiringOrg TEXT,
	    Identifier TEXT,
	    Title TEXT,
	    validThrough TEXT,
	    currency TEXT,
	    maxValue DECIMAL, 
		minValue DECIMAL);
	`)
	if err != nil {
		return nil, fmt.Errorf("%s: %w", op, err)
	}

	_, err = stmt.Exec()
	if err != nil {
		return nil, fmt.Errorf("%s: %w", op, err)
	}

	return &Storage{db: db}, nil

}

func (s *Storage) CreateVac(vacancy model.Vacancy) error {
	const op = "storage.sqlite.CreateVac"

	stmt, err := s.db.Prepare("INSERT INTO vacancy(link, dataPosted, description, employmentType, hiringOrg, Identifier, Title, validThrough, currency, maxValue, minValue) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)")
	if err != nil {
		return fmt.Errorf("%s: %w", op, err)
	}

	_, err = stmt.Exec(vacancy.Link, vacancy.DatePosted, vacancy.Description, vacancy.EmploymentType, vacancy.OrganizationSite, vacancy.OrganizationName, vacancy.Title, vacancy.ValidThrough, vacancy.BaseSalary.Currency, vacancy.BaseSalary.Value.MaxValue, vacancy.BaseSalary.Value.MinValue)
	if err != nil {
		/*if sqliteErr, ok := err.(sqlite3.Error); ok && sqliteErr.ExtendedCode == sqlite3.ErrConstraintUnique {
			return fmt.Errorf("%s: %w", op, storage.ErrVacancyExists)
		}*/
		return fmt.Errorf("%s: %w", op, err)
	}

	return nil
}

func (s *Storage) GetByID(id int) (model.Vacancy, error) {
	const op = "storage.sqlite.GetByID"

	stmt, err := s.db.Prepare(
		"SELECT link, dataPosted, description, employmentType, hiringOrg, Identifier, Title, validThrough, currency, maxValue, minValue FROM vacancy WHERE id = ?")
	if err != nil {
		if errors.Is(err, sql.ErrNoRows) {
			return model.Vacancy{}, fmt.Errorf("%s: prepare statment: %w", op, err)
		}
	}

	var resVac model.Vacancy
	err = stmt.QueryRow(id).Scan(&resVac.Link, &resVac.DatePosted, &resVac.Description, &resVac.EmploymentType, &resVac.OrganizationSite, &resVac.OrganizationName, &resVac.Title, &resVac.ValidThrough, &resVac.BaseSalary.Currency, &resVac.BaseSalary.Value.MaxValue, &resVac.BaseSalary.Value.MinValue)
	if err != nil {
		if errors.Is(err, sql.ErrNoRows) {
			return model.Vacancy{}, fmt.Errorf("vacancy not found")
		}
		return model.Vacancy{}, fmt.Errorf("%s execute statment: %w", op, err)
	}

	return resVac, nil

}

func (s *Storage) GetList() ([]model.Vacancy, error) {
	const op = "storage.sqlite.GetList"

	stmt, err := s.db.Prepare("SELECT link, dataPosted, description, employmentType, hiringOrg, Identifier, Title, validThrough, currency, maxValue, minValue FROM vacancy;")
	if err != nil {
		if errors.Is(err, sql.ErrNoRows) {
			return nil, fmt.Errorf("%s: prepare statment: %w", op, err)
		}
	}
	resVac := []model.Vacancy{}
	rows, err := stmt.Query()
	if err != nil {
		if errors.Is(err, sql.ErrNoRows) {
			return nil, fmt.Errorf("%s: vacancy not found", op)
		}
		return nil, fmt.Errorf("%s execute statment: %w", op, err)
	}
	defer rows.Close()
	for rows.Next() {
		tmpVac := model.Vacancy{}
		err := rows.Scan(&tmpVac.Link, &tmpVac.DatePosted, &tmpVac.Description, &tmpVac.EmploymentType, &tmpVac.OrganizationSite, &tmpVac.OrganizationName, &tmpVac.Title, &tmpVac.ValidThrough, &tmpVac.BaseSalary.Currency, &tmpVac.BaseSalary.Value.MaxValue, &tmpVac.BaseSalary.Value.MinValue)
		if err != nil {
			return nil, fmt.Errorf("%s scan error: %w", op, err)
		}
		resVac = append(resVac, tmpVac)
	}

	if err := rows.Err(); err != nil {
		return nil, fmt.Errorf("%s iteration error: %w", op, err)
	}

	return resVac, nil
}

func (s *Storage) Delete(id int) error {

	const op = "storage.sqlite.Delete"

	stmt, err := s.db.Prepare("DELETE FROM vacancy WHERE id = ?")
	if err != nil {
		if errors.Is(err, sql.ErrNoRows) {
			return fmt.Errorf("%s: prepare statment: %w", op, err)
		}
	}

	res, err := stmt.Exec(id)
	if err != nil {
		return fmt.Errorf("%s execute statment: %w", op, err)
	}

	count, err := res.RowsAffected()
	if count == 0 {
		return fmt.Errorf("vacancy with this ID not found")
	}

	return nil
}

func (s *Storage) Close() error {
	const op = "storage.sqlite.Close"
	err := s.db.Close()
	if err != nil {
		return fmt.Errorf("%s: %w", op, err)
	}
	return nil
}

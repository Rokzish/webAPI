package storage

import "errors"

var (
	ErrVacancyExists = errors.New("vacancy exists")
)

package model

import (
	"encoding/json"
	"fmt"
	"strings"
)

func (v *Vacancy) Marshal() ([]byte, error) {
	return json.Marshal(v)
}

type Vacancy struct {
	DatePosted       string     `json:"datePosted"`
	Description      string     `json:"description"`
	EmploymentType   string     `json:"employmentType"`
	Title            string     `json:"title"`
	ValidThrough     string     `json:"validThrough"`
	BaseSalary       BaseSalary `json:"baseSalary,omitempty"`
	Link             string     `json:"link,omitempty"`
	OrganizationName string     `json:"organizationName"`
	OrganizationSite string     `json:"organizationSite"`
}

type BaseSalary struct {
	Currency string `json:"currency"`
	Value    Value  `json:"value"`
}

type Value struct {
	MaxValue float64 `json:"maxValue,omitempty"`
	MinValue float64 `json:"minValue,omitempty"`
}

func (v *Vacancy) UnmarshalJSON(data []byte) error {
	const op = "model.vacancy.unmarshalJSON"
	var container map[string]interface{}
	err := json.Unmarshal(data, &container)
	if err != nil {
		return fmt.Errorf("%s: %w", op, err)
	}
	if DatePosted, ok := container["datePosted"]; ok {
		v.DatePosted = DatePosted.(string)
	} else {
		v.DatePosted = ""
	}

	if EmploymentType, ok := container["employmentType"]; ok {
		v.EmploymentType = EmploymentType.(string)
	} else {
		v.EmploymentType = ""
	}

	if hiringOrganization, ok := container["hiringOrganization"]; ok {
		if sameAs, ok2 := hiringOrganization.(map[string]interface{})["sameAs"]; ok2 {
			v.OrganizationSite = sameAs.(string)
		} else {
			v.OrganizationSite = ""
		}
	} else {
		v.OrganizationSite = ""
	}

	if identifier, ok := container["identifier"]; ok {
		if name, ok2 := identifier.(map[string]interface{})["name"]; ok2 {
			v.OrganizationName = name.(string)
		} else {
			v.OrganizationName = ""
		}
	} else {
		v.OrganizationName = ""
	}

	if Title, ok := container["title"]; ok {
		v.Title = Title.(string)
	} else {
		v.EmploymentType = ""
	}

	if ValidThrough, ok := container["validThrough"]; ok {
		v.ValidThrough = ValidThrough.(string)
	} else {
		v.EmploymentType = ""
	}

	if baseSalary, ok := container["baseSalary"]; ok {
		if currency, ok2 := baseSalary.(map[string]interface{})["currency"]; ok2 {
			v.BaseSalary.Currency = currency.(string)
		} else {
			v.BaseSalary.Currency = ""
		}
		if value, ok2 := baseSalary.(map[string]interface{})["value"]; ok2 {
			if maxValue, ok3 := value.(map[string]interface{})["maxValue"]; ok3 {
				v.BaseSalary.Value.MaxValue = maxValue.(float64)
			} else {
				v.BaseSalary.Value.MaxValue = 0
			}
			if minValue, ok3 := value.(map[string]interface{})["minValue"]; ok3 {
				v.BaseSalary.Value.MinValue = minValue.(float64)
			} else {
				v.BaseSalary.Value.MinValue = 0
			}
		}
	}

	if description, ok := container["description"]; ok {
		text := description.(string)
		text = strings.ReplaceAll(text, "</li>", " ")
		text = strings.ReplaceAll(text, "<li>", " ")
		text = strings.ReplaceAll(text, "<p>", " ")
		text = strings.ReplaceAll(text, "</p>", " ")
		text = strings.ReplaceAll(text, "</ul>", " ")
		text = strings.ReplaceAll(text, "<ul>", " ")
		text = strings.ReplaceAll(text, "<em>", " ")
		text = strings.ReplaceAll(text, "</em>", " ")
		text = strings.ReplaceAll(text, "</ol>", " ")
		text = strings.ReplaceAll(text, "<ol>", " ")
		text = strings.ReplaceAll(text, "<br>", " ")
		text = strings.ReplaceAll(text, "</br>", " ")
		text = strings.ReplaceAll(text, "<strong>", " ")
		text = strings.ReplaceAll(text, "</strong>", " ")
		text = strings.ReplaceAll(text, "<h3>", " ")
		text = strings.ReplaceAll(text, "</h3>", " ")

		text = strings.ReplaceAll(text, "    ", " ")
		v.Description = text
	} else {
		v.Description = ""
	}

	return nil
}

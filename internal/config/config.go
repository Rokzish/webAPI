package config

import (
	"github.com/ilyakaznacheev/cleanenv"
	"log"
	"os"
	"time"
)

type Config struct {
	StoragePath string `yaml:"storage_path"`
	HTTPServer  `yaml:"http_server"`
	DBUrl       string `yaml:"db_url"`
	SeleniumUrl string `yaml:"selenium_url"`
}

type HTTPServer struct {
	Address     string        `yaml:"address" env-default:"localhost:8080"`
	Timeout     time.Duration `yaml:"timeout" env-default:"4s"`
	IdleTimeout time.Duration `yaml:"idle_timeout" env-default:"60s"`
}

func MustLoad() *Config {
	const op = "config.MustLoad"
	configPath := os.Getenv("CONFIG_PATH")
	if configPath == "" {
		log.Fatalf("%s: CONFIG_PATH is not set", op)
	}

	if _, err := os.Stat(configPath); os.IsNotExist(err) {
		log.Fatalf("%s: config file does not exist: %s", op, configPath)
	}

	var cfg Config
	if err := cleanenv.ReadConfig(configPath, &cfg); err != nil {
		log.Fatalf("%s: cannot read config: %s", op, err)
	}

	return &cfg
}

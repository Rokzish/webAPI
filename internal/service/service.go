package service

import (
	"encoding/json"
	"fmt"
	"github.com/PuerkitoBio/goquery"
	"github.com/tebeka/selenium"
	"gitlab.com/Rokzish/webAPI/internal/lib/logger/sl"
	"gitlab.com/Rokzish/webAPI/internal/model"
	"golang.org/x/exp/slog"
	"net/http"
	"strconv"
	"strings"
	"sync"
	"time"
)

const (
	maxTries         = 15
	HabrCareerLink   = "https://career.habr.com"
	countPageVacancy = 25
)

type Storage interface {
	CreateVac(vacancy model.Vacancy) error
	GetByID(id int) (model.Vacancy, error)
	GetList() ([]model.Vacancy, error)
	Delete(id int) error
	Close() error
}

type VacancyController struct {
	storage Storage
	wd      selenium.WebDriver
}

func NewVacancyController(storage Storage, wd selenium.WebDriver) *VacancyController {
	return &VacancyController{storage: storage, wd: wd}
}

func (v *VacancyController) UpdateVacancyList(log *slog.Logger, query string) (int, error) {
	const op = "service.UpdateVacancyList"

	var vacancyCount int
	var err error
	for i := 0; i < maxTries; {
		vacancyCount, err = getVacancyCount(v.wd, query)
		if err != nil {
			log.Warn(op, sl.Err(err))

			i++
			continue
		}
		break
	}

	pages := vacancyCount / countPageVacancy
	isDiv := vacancyCount%countPageVacancy == 0
	if !isDiv {
		pages++
	}
	links := make([]string, 0)

	for i := 1; i <= pages; i++ {
		for j := 0; j < maxTries; {
			link, err := getLinksFromPage(log, v.wd, i, query)
			if err != nil {
				log.Warn(op, sl.Err(err))
				j++
				continue
			}
			links = append(links, link...)
			break

		}
	}
	var availableVacancy int
	var vacancies []model.Vacancy
	var wg sync.WaitGroup
	var mutex sync.Mutex
	for i, link := range links {
		wg.Add(1)
		go func(link string) {
			vac, err := getDataFromLink(link)
			if err != nil {
				log.Warn(op, slog.String("source", link), sl.Err(err))
				wg.Done()
				return
			}
			mutex.Lock()
			availableVacancy++
			vacancies = append(vacancies, vac)
			mutex.Unlock()
			wg.Done()
		}(link)
		///time.Sleep(time.Millisecond * 500)
		if i%10 == 0 {
			time.Sleep(time.Millisecond * 500)
		}
	}
	wg.Wait()

	for _, vac := range vacancies {
		err := v.storage.CreateVac(vac)
		if err != nil {
			log.Warn(op, slog.String("source", vac.Link), sl.Err(err))
		}
	}
	return availableVacancy, nil
}

func getVacancyCount(wd selenium.WebDriver, query string) (int, error) {
	const op = "service.getVacancyCount"
	if err := wd.Get(fmt.Sprintf("https://career.habr.com/vacancies?page=1&q=%s&type=all", query)); err != nil {
		return 0, err
	}

	elem, err := wd.FindElement(selenium.ByCSSSelector, ".search-total")
	if err != nil {
		return 0, fmt.Errorf("%s: %w", op, err)
	}
	vacancyCountRaw, err := elem.Text()
	if err != nil {
		return 0, fmt.Errorf("%s: %w", op, err)
	}

	vacancyCountSlice := strings.Split(vacancyCountRaw, " ")
	count, err := strconv.Atoi(vacancyCountSlice[1])
	if err != nil {
		return 0, fmt.Errorf("%s: %w", op, err)
	}

	return count, nil
}

func getLinksFromPage(log *slog.Logger, wd selenium.WebDriver, page int, query string) ([]string, error) {
	const op = "service.getLinksFromPage"
	if err := wd.Get(fmt.Sprintf("https://career.habr.com/vacancies?page=%d&q=%s&type=all", page, query)); err != nil {
		return nil, fmt.Errorf("%s: %w", op, err)
	}

	elems, err := wd.FindElements(selenium.ByCSSSelector, ".vacancy-card__title-link")
	if err != nil {
		return nil, fmt.Errorf("%s: %w", op, err)
	}
	var links []string
	for i := range elems {
		var link string
		link, err = elems[i].GetAttribute("href")
		if err != nil {
			log.Warn(op, sl.Err(err))
			continue
		}
		links = append(links, HabrCareerLink+link)

	}

	return links, nil
}

func getDataFromLink(link string) (model.Vacancy, error) {
	const op = "service.getDataFromLink"
	resp, err := http.Get(link)
	if err != nil {
		return model.Vacancy{}, err
	}
	if resp.StatusCode == http.StatusNotFound {
		return model.Vacancy{}, fmt.Errorf("%s link content not available", op)
	}

	var doc *goquery.Document
	doc, err = goquery.NewDocumentFromReader(resp.Body)
	if err != nil && doc != nil {
		return model.Vacancy{}, fmt.Errorf("%s: %w", op, err)
	}

	dd := doc.Find("script[type=\"application/ld+json\"]")
	if dd == nil {
		return model.Vacancy{}, fmt.Errorf("%s: habr vacancy nodes not found", op)
	}
	ss := dd.First().Text()

	var vacancy model.Vacancy
	vacancy.Link = link
	err = json.Unmarshal([]byte(ss), &vacancy)
	if err != nil {
		return model.Vacancy{}, fmt.Errorf("%s reading error: %w", op, err)
	}
	return vacancy, nil

}

func (v *VacancyController) DeleteVacancy(id int) error {
	return v.storage.Delete(id)
}

func (v *VacancyController) GetVacancyByID(id int) (model.Vacancy, error) {
	return v.storage.GetByID(id)
}

func (v *VacancyController) GetList() ([]model.Vacancy, error) {
	return v.storage.GetList()
}
